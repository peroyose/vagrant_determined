# from https://docs.determined.ai/latest/how-to/install-main.html

echo "start install_determined.sh"

source /vagrant/determined_config.sh



docker pull postgres:10
docker pull determinedai/determined-master:$determined_version
docker pull determinedai/determined-agent:$determined_version

# create the determined network
docker network create determined


# the agent need pytorch etc. this is encasulated in a environment, let's pull them here
# if not the agent will pull it by their own, so you can comment this out savely.
#docker pull determinedai/environments:py-3.6.9-pytorch-1.4-tf-1.14-cpu-0.4.0

