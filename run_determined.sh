#!/bin/bash


# TODO replace by the docker compose file services.yaml
echo "start run_determined.sh"

cd /vagrant/determined-master
docker-compose up -d

sleep 100  # master must start before we can connect with the agent

cd /vagrant/determined-agent
docker-compose up -d

# from https://docs.determined.ai/latest/how-to/install-main.html


# source /vagrant/determined_config.env


# # su vagrant

# network="determined"

# docker run -d \
#     --name determined-db \
#     --network $network \
#     -p 5432:5432 \
#     -v determined_db:/var/lib/postgresql/data \
#     -e POSTGRES_DB=determined \
#     -e POSTGRES_PASSWORD=$dbpasswort \
#     postgres:10



# docker run  -d \
#     --name determined-master \
#     --network $network \
#     -v /vagrant/determined/master.yaml:/etc/determined/master.yaml \
#     -v /var/run/docker.sock:/var/run/docker.sock \
#     -p $determined_master_port:$determined_master_port \
#     -e DET_DB_HOST=determined-db \
#     -e DET_DB_NAME=determined \
#     -e DET_DB_PORT=5432 \
#     -e DET_DB_USER=postgres \
#     -e DET_DB_PASSWORD=$dbpasswort \
#     -e DET_HTTP_PORT=$determined_master_port  \
#     determinedai/determined-master:$determined_version

# #  -e INTEGRATIONS_HOST_PORT=8080 \

# sleep 10  # master must start before we can connect with the agent




# docker run  -d  \
#    --name determined-agent \
#    --network $network \
#    -v /vagrant/determined/agent.yaml:/etc/determined/agent.yaml \
#    -v /var/run/docker.sock:/var/run/docker.sock \
#    -e DET_MASTER_HOST=10.0.2.15 \
#    -e DET_MASTER_PORT=$determined_master_port \
#    determinedai/determined-agent:$determined_version


#   --network $network \

#pip3 install determined-deploy
#sudo -i -u vagrant /bin/bash << EOF
#export PATH=$PATH:/home/vagrant/.local/bin/
#det-deploy local cluster-up --no-gpu #also starts a agent
##det-deploy local agent-up --no-gpu localhost
#EOF


echo "end run_determined"
