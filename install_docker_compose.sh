# for releases/versions see
# https://github.com/docker/compose/releases

version="1.26.0"

URL="https://github.com/docker/compose/releases/download/$version/docker-compose-$(uname -s)-$(uname -m)"

curl -L $URL -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


# are the bash-completions installed??
# curl -L https://raw.githubusercontent.com/docker/compose/$version/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose



