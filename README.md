

This is work in progress. 


## Vagrant setup for determined.ai


This is a minimal Vagrant configuration for determined.ai. 


- installs a linux environment (e.g. ubuntu18) in a virtal machine (e.g. virtaulbox).
- installs a docker environment in the virtual machine
- installs determined.ai docker containers (postgre, master, agent) in the docker environment
- installs determined.ai python, pyortch and tensorflow in a docker container (takes some time)
- runs the determined docker container


#### Installing
```
> vagrant up
```

On your host machine in the determined_pytorch/minist_pytorch directory:
```
> export DET_MASTER=localhost:8080
> det experiment create const.yaml .
```

#### Prerequisite

You must have installed vagrant and virtualbox, see e.g. https://linuxize.com/post/how-to-install-vagrant-on-ubuntu-18-04/
If you want to change the provider, just edit the Vagrantfile.


#### Known Issues


- The master address is 8080. How to configure the determined master port??

#### Web Interface

You can log-in via a web-interface in you browser: http://localhost:8080/
- User: determined or admin
- Password: -leave it blank-


#### Debugging tips


inside the virtual environment (`vagrant ssh`) you can

- check if all all containers are running by `docker ps`
- see the docker logs by `docker log`, e.g.:
```
> docker logs determined-master
```
- docker deamon log (in systemd linux systems, e.g. ubuntu 18):
```
> journalctl -u docker.service
```

- login into a container, e.g. `docker exec -ti determined-db ping determined-master`
  - and e.g. testining if the network works by installing ping 'apt-get update' 'apt-get install iputils-ping'.


